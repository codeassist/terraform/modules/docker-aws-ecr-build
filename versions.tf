terraform {
  required_version = "~> 0.12.0"

  required_providers {
    aws      = "~> 2.0"
    external = "~> 1.2"
    null     = "~> 2.1"
  }
}
