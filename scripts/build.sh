#!/usr/bin/env sh

# Exit immediately if a command exits with a non-zero status.
set -e

# Check script arguments and override defaults
context_dir="."
dockerfile="Dockerfile"
repository_url="local/docker-aws-ecr-build"
image_tag="latest"
build_opts=""
if [ ${#} -ne 0 ]; then
  if [ ! -z "$1" ]; then
    context_dir="$1";
  fi;
  if [ ! -z "$2" ]; then
    dockerfile="$2";
  fi;
  if [ ! -z "$3" ]; then
    repository_url="$3";
  fi;
  if [ ! -z "$4" ]; then
    image_tag="$4";
  fi;
  if [ ! -z "$5" ]; then
    build_opts="$5";
  fi;
fi

# Debug output
echo "`date +'%F %T'` - [*INFO*]: going to execute [docker build ${build_opts} -t ${repository_url}:${image_tag} -f ${dockerfile} ${context_dir}]."

# Check AWS CLI is installed
which aws > /dev/null \
  || { \
        echo "`date +'%F %T'` - [*ERROR*]: could not find 'aws-cli' installed!"; \
        exit 1; \
    }

# Check that Docker daemon is installed and accessible
which docker > /dev/null \
  && docker ps > /dev/null \
    || { \
          export DOCKER_HOST="tcp://127.0.0.1:2375" &&
          docker ps > /dev/null \
          || { \
                 echo "`date +'%F %T'` - [*ERROR*]: docker daemon is not running or you may have no permission to access it!" ; \
                 exit 1; \
             }
       }

# Login to the AWS ECR
$(aws --region ${AWS_REGION} ecr get-login --no-include-email) \
  || { \
      echo "`date +'%F %T'` - [*ERROR*]: ECR login failed!"; \
      exit 1; \
    }

# Debug output
echo "`date +'%F %T'` - [*INFO*]: building from [${dockerfile}] using [${context_dir}] as the root of the context and tag as [${repository_url}:${image_tag}]."
# Build image
docker build ${build_opts} -t ${repository_url}:${image_tag} -f ${dockerfile} ${context_dir}

# Debug output
echo "`date +'%F %T'` - [*INFO*]: pushing image to [${repository_url}:${image_tag}]."
# Push image
docker push ${repository_url}:${image_tag}

## Debug output
#echo "`date +'%F %T'` - [*INFO*]: logging out from the repo."
## Try to logout from the repo (supress error as it's not strictly required)
#docker logout ${repository_url%%/*} \
#  || true
