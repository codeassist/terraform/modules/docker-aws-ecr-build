# docker-aws-ecr-build

The module to build docker images locally and pushes them into AWS ECR with `shell` scripts using AWS CLI tool.

## Requirements
- Terraform 0.12
- The following programs must be installed and available in system `$PATH`:
    * `sh`
    * `aws`
    * `docker`

To get module to build an image:
- a Docker daemon needs to be running
- DOCKER_HOST env variable must be set (e.g. DOCKER_HOST="tcp://127.0.0.1:2375")

## License
MIT
