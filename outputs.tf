output "image_url" {
  description = "Full URL to the image in the AWS ECR repository."
  value       = "${var.docker_ecr_build_repository_url}:${var.docker_ecr_build_image_tag}"
}

output "build_id" {
  description = "An unique build ID to use as dependecy for other resources."
  value       = join("", null_resource.build.*.id)
}