# --------------------------
# Common/General parameters
# --------------------------
variable "docker_ecr_build_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # "false" prevents the module from creating any resources
  default = false
}
variable "docker_ecr_build_module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  default     = null
}
variable "docker_ecr_build_watch_changed_files" {
  description = "The list of files when content changed a new build is triggered."
  type        = list(string)
  default     = []
}


# ------------------------
# Docker build parameters
# ------------------------
variable "docker_ecr_build_context_dir" {
  description = "This directory is the root of the build context."
  type        = string
  default     = "."
}
variable "docker_ecr_build_dockerfile" {
  description = "The path to the Dockerfile."
  type        = string
  default     = "Dockerfile"
}
variable "docker_ecr_build_image_tag" {
  description = "The tag which will be used for the image."
  type        = string
  default     = "latest"
}
variable "docker_ecr_build_opts" {
  description = "Additional build options like ['--no-cache']"
  type        = list(string)
  default     = []
}

# -------------------
# AWS ECR parameters
# -------------------
variable "docker_ecr_build_aws_region" {
  description = "AWS region for ECR."
  type        = string
  default     = "us-east-1"
}
variable "docker_ecr_build_repository_url" {
  description = "(Required) The URL of the repository (in the form <aws_account_id>.dkr.ecr.<aws_region>.amazonaws.com/<repository_name>)"
  type        = string
}
