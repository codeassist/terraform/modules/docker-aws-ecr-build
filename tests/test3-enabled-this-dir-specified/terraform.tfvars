# --------------------------
# Common/General parameters
# --------------------------
# Whether to create the resources ("false" prevents the module from creating any resources).
docker_ecr_build_module_enabled = true
# Emulation of `depends_on` behavior for the module (non zero length string can be used to have current module
# wait for the specified resource.)
docker_ecr_build_module_depends_on = [
  "dependency-1"
]
# The list of files when content changed a new build is triggered.
docker_ecr_build_watch_changed_files = [
  "./Dockerfile"
]

# ------------------------
# Docker build parameters
# ------------------------
# This directory is the root of the build context.
docker_ecr_build_context_dir = "./test3-enabled-this-dir-specified"
# The path to the Dockerfile.
docker_ecr_build_dockerfile = "Dockerfile"
# The tag which will be used for the image.
docker_ecr_build_image_tag = "latest"

# -------------------
# AWS ECR parameters
# -------------------
# AWS region for ECR.
docker_ecr_build_aws_region = "us-east-1"
# (Required) The URL of the repository (in the form
# <aws_account_id>.dkr.ecr.<aws_region>.amazonaws.com/<repository_name>)
docker_ecr_build_repository_url = "<aws_account_id>.dkr.ecr.<aws_region>.amazonaws.com/<repository_name>"
