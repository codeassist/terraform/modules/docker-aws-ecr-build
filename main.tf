locals {
  enabled = var.docker_ecr_build_module_enabled ? true : false
}

# ------------------------------------------------------------------------------
# Execute script to build specified Dockerfile and push to the AWS ECR registry
# ------------------------------------------------------------------------------
resource "null_resource" "build" {
  # The null_resource resource implements the standard resource lifecycle but takes no further action.
  # The triggers argument allows specifying an arbitrary set of values that, when changed, will cause the resource
  # to be replaced.
  count = local.enabled ? 1 : 0

  depends_on = [
    var.docker_ecr_build_module_depends_on
  ]

  # (Optional) A map of arbitrary strings that, when changed, will force the null resource to be replaced, re-running
  # any associated provisioners.
  triggers = {
    # uuid() generates a unique identifier string.
    # The id is a generated and formatted as required by RFC 4122 section 4.4, producing a Version 4 UUID. The result
    # is a UUID generated only from pseudo-random numbers.
    # This function produces a new value each time it is called, and so using it directly in resource arguments will
    # result in spurious diffs.
    frequently_changed_string = length(var.docker_ecr_build_watch_changed_files) > 0 ? md5(join("", [for file_to_monitor in var.docker_ecr_build_watch_changed_files : filemd5(file_to_monitor)])) : uuid()
    image_tag = var.docker_ecr_build_image_tag
    build_opts = join(" ", var.docker_ecr_build_opts)
    repository = var.docker_ecr_build_repository_url
  }

  provisioner "local-exec" {
    # (Required) This is the command to execute. It can be provided as a relative path to the current working
    # directory or as an absolute path. It is evaluated in a shell, and can use environment variables or Terraform
    # variables.
    command = "${path.module}/scripts/build.sh ${var.docker_ecr_build_context_dir} ${var.docker_ecr_build_dockerfile} ${var.docker_ecr_build_repository_url} ${var.docker_ecr_build_image_tag} '${join(" ", var.docker_ecr_build_opts)}'"
    # (Optional) block of key value pairs representing the environment of the executed command.
    # Inherits the current process environment.
    environment = {
      AWS_DEFAULT_REGION = var.docker_ecr_build_aws_region
      AWS_REGION = var.docker_ecr_build_aws_region
    }
  }
}

